package com.example;

import java.util.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.Import;

@SpringBootApplication

@EnableAutoConfiguration
@EnableDiscoveryClient
@Import(AccountsConfiguration.class)
public class MavenHelloWorldAccountsApplication {

    @Autowired
    protected AccountRepository accountRepository;

    protected Logger logger = Logger.getLogger(MavenHelloWorldAccountsApplication.class.getName());

    public static void main(String[] args) {
        SpringApplication.run(MavenHelloWorldAccountsApplication.class, args);
    }
}
